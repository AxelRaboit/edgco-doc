 **Note: This doc is the first try to implement a theme into an existing view to overload assets, so this is just to start soemthing about this and it should be improve **

# Implementation of a specific template that extends from a basic template and brings asset overrides
**Note:**
- The templates for this type of request are the templates based from the app and not from the seed-bundle
- The Templates are in Bootstrap V5 and the project in Bootstrap V4

**Repository of the prototype used**
```
https://gitlab.com/edgco_project/edgco_website_seed/edgco_website_seed_proto-btv5/-/tree/seed-immo
```

## Placement of our assets
**A template folder is present to store our templates in categorized folders**
```
Folder: app/public/assets/templates/immo
```
In this folder we put the following folders / files\
```
    - css/index.js
    - scss/index.scss
    - fonts/...
    - js/index.js
```
These folders/files will allow template asset overload

## Add file and data from the prototype
- Added fonts to our **fonts** folder
```
https://gitlab.com/edgco_project/edgco_website_seed/edgco_website_seed_proto-btv5/-/tree/seed-immo/assets/fonts
```

- Add css in our css/index.css file (And font linking)
```
https://gitlab.com/edgco_project/edgco_website_seed/edgco_website_seed_proto-btv5/-/blob/seed-immo/assets/css/main.css

```
## Communication of elements with Webpack
In our index.js file located in **public/assets/templates/immo/js/index.js** we will import our file **css/index.css**
```javascript
// Exemple avec l'import du fichier css et bootstrap
require('../css/index.css');
require('bootstrap');
require('popper.js');

import 'bootstrap/dist/css/bootstrap.css';
```

## Template creation
**Our twig template is placed in app/templates/immo/page.html.twig**

In the latter we will extend from a twig base, and extend the **stylesheets** and **javascripts** blocks
```twig
{% extends '@SeedBundle/Elements/Pages/page-simple.html.twig' %}
 
{% block stylesheets %}
    {{ encore_entry_link_tags('template/immo') }}
{% endblock %}
 
{% block javascripts %}
    {{ encore_entry_script_tags('template/immo') }}
{% endblock %}
```

## Linking our work to Webpack by adding an addEntry
```javascript
// Templates/immo
    .addEntry('template/immo', ['./public/assets/templates/<myTheme>/css/index.css', './public/assets/templates/<myTheme>/js/index.js'])
```

## Adding html code in the top / bottom contents of a page's BO
Inject the HTML code into the content via **Tools > Source code**

**Note: Do not inject the nav, header, footer of the prototype. Main content only.**

```
https://gitlab.com/edgco_project/edgco_website_seed/edgco_website_seed_proto-btv5/-/blob/seed-immo/html/pageStandardWithImage.html
```

## Use the template we created
In the Dev section of the page, add the template (When the template does not come from Seed, we must start as if we were in the app/templates folder)
```
immo/page.html.twig
```

### Expected Result
![image](./images/template-expected.png)

### Obtained result
![image](./images/template-result.png)

## Comments


**Remarks**
- The blue block does not touch the edges, it seems to be contained in a bootstrap class "container" and not "container-fluid"

- Same thing for the gray block which is similar

- Material icons is not imported, which is basically imported in the head of an html base for example.

- The navbar does not have the right size (Wider)

- The banner / header does not take the supposed size (It is imported from the seed bundle before overloading)

**Good Points**
- Bootstrap works

- The css file is well used as well as the fonts which are defined in the css and are imported

## Files related to the view being extended (@SeedBundle/Elements/Pages/page-simple.html.twig)
```
- app/lib/seed-bundle/templates/Elements/Pages/page-simple.html.twig\
- app/lib/seed-bundle/public/assets/frontend/css/index.css\
- app/lib/seed-bundle/public/assets/frontend/css/main.css
```
