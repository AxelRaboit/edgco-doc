# Create form from an existing form (extended form)

## Situation

**We want to create a form with**
```
⚫️ Text inputs
⚫️ Select inputs
⚫️ Document upload
⚫️ Image upload

```
## Creation of constants
**1 - Constants declaration of pags and error message**
```
📃 app/src/Enum/DataEnum.php
```
```
💡 In this part we will define all the constants to display error messages, pages, and select options
```
```php
// Constant for the contact page
public const DATA_PAGE_CONTACT_ADVANCED_DEV_KEY = 0;

// Constant for the page in case of success
public const DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY = 2;

// Constant with select options
public const DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS = 1;

// Constant error message (globally way)
public const DATA_MESSAGE_CONTACT_ADVANCED_ERROR = 3;

// Constant error message in case we didn't choose a value
public const DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR = 4;

// Constant error message image format
public const DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR = 5;

// Constant error message image size
public const DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR = 6;

// Constant error message file format
public const DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR = 7;

// Constant error message file size
public const DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR = 8;
```
**And declare every constants with their content**
```php
/** @var array */
    public static $data = [
        self::DATA_PAGE_CONTACT_ADVANCED_DEV_KEY => [
            // Name: Which is the name of the constant that we will find into the back office
            'name' => 'DATA_PAGE_CONTACT_ADVANCED_DEV_KEY',
            // dev_key: Ref to the constant on the top
            'dev_key' => self::DATA_PAGE_CONTACT_ADVANCED_DEV_KEY,
            // Default value, in this page case we assign a default page that we will change the value in the back office after for associate a page
            'value' => PageEnum::PAGE_CONTACT_ADVANCED_DEV_KEY_DEFAULT,
            // System data
            'is_system' => true,
            // Sensitive aata
            'is_sensitive' => false,
            // Category name to find the constant into the back office
            'category' => 'Pages',
        ],
        //...
    ];
```

**Example**
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 * Data - Enum.
 * Becarefull, key from 10001 are used by SeedDataEnum.
 * To complete if needed.
 */
class DataEnum
{
    //Last key is 8

    public const DATA_PAGE_CONTACT_ADVANCED_DEV_KEY = 0;
    public const DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY = 2;

    public const DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS = 1;

    public const DATA_MESSAGE_CONTACT_ADVANCED_ERROR = 3;
    public const DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR = 4;

    public const DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR = 5;
    public const DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR = 6;

    public const DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR = 7;
    public const DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR = 8;

    /** @var array */
    public static $data = [
        self::DATA_PAGE_CONTACT_ADVANCED_DEV_KEY => [
            'name' => 'DATA_PAGE_CONTACT_ADVANCED_DEV_KEY',
            'dev_key' => self::DATA_PAGE_CONTACT_ADVANCED_DEV_KEY,
            'value' => PageEnum::PAGE_CONTACT_ADVANCED_DEV_KEY_DEFAULT,
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Pages',
        ],
        self::DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS => [
            'name' => 'DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS',
            'dev_key' => self::DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS,
            'value' => 'item 1;item 2;item 3',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Contact',
            'help' => 'Options séparées par des \';\'',
        ],
        self::DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY => [
            'name' => 'DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY',
            'dev_key' => self::DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY,
            'value' => PageEnum::PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY_DEFAULT,
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Pages',
        ],
        self::DATA_MESSAGE_CONTACT_ADVANCED_ERROR => [
            'name' => 'DATA_MESSAGE_CONTACT_ADVANCED_ERROR',
            'dev_key' => self::DATA_MESSAGE_CONTACT_ADVANCED_ERROR,
            'value' => 'Une erreur s\'est produite.',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
        self::DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR,
            'value' => 'Veuillez selectionner une valeur',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
        self::DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR,
            'value' => 'Format du fichier incorrect (Acceptés: PNG / JPEG)',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
        self::DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR,
            'value' => 'Taille du fichier trop volumineux (Maximum: 1mo)',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
        self::DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR,
            'value' => 'Format du fichier incorrect (Accepté: PDF)',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
        self::DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR,
            'value' => 'Taille du fichier trop volumineux (Maximum: 2mo)',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
    ];

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```
**2 - Go to the PageEnum**
```
📃 app/src/Enum/PageEnum.php
```
```
💡 In this part we will define all the constants to give default page to the previous part
```
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 *  Page - Enum.
 */
class PageEnum
{
    public const PAGE_CONTACT_ADVANCED_DEV_KEY_DEFAULT = 1;
    public const PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY_DEFAULT = 2;

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```

**3 - Go to the MessengerTemplateEmailEnum**
```
📃 app/src/Enum/MessengerTemplateEmailEnum.php
```
**3.1 - Constants declaration of the messenger template email**
```
💡 These constants will be available in the back office after declarations
```
**Constant's content**
```php
// Constant related to the email template taht we will find into the back office after declaration
public const TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT = 2;

/** @var array */
    public static $data = [
        self::TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT => [
            // Name: Which is the name of the constant that we will find into the back office
            'name' => 'TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT',
            // dev_key: Ref to the constant on the top
            'dev_key' => self::TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT,
            // Mail subject
            'subject' => 'Contact',
            // Receivers email
            'receivers' => 'receiver@mon-site.fr',
            // Data system
            'is_system' => false,
            // Active notification
            'is_notification' => false,
            // Sender email
            'sender_email' => 'sender@mon-site.fr',
            // Helper
            'help' => '<p>{{ message }}<br /><br /><b>Nom et pr&eacute;nom : </b> {{ lastname }} {{ firstname }}<br /><br /><b>Email : </b> {{ email }}<br /><br /><b>T&eacute;l&eacute;phone : </b> {{ phonenumber }}<br /><br /><b>Sujet avancée : </b> {{ subject }}<br /><br /><b>Image : </b>{{ image }}<br /><br /><b>Document : </b>{{ document }}</p>',
            // Email content (This is the email structure, the variables will be injected inside "{{ content }}" )
            'content' => '<p>{{ message }}<br /><br /><b>Nom et pr&eacute;nom : </b> {{ lastname }} {{ firstname }}<br /><br /><b>Email : </b> {{ email }}<br /><br /><b>T&eacute;l&eacute;phone : </b> {{ phonenumber }}<br /><br /><b>Sujet avancée : </b> {{ subject }}<br /><br /><b>Image : </b>{{ image }}<br /><br /><b>Document : </b>{{ document }}</p>',
        ],
        //...
    ];
```
**Example**
```
❗ In this example we can see TEMPLATE_EMAIL_CONTAC_SENT which is the template related to the form that we want to extend
```
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 *  MessengerTemplateEmail - Enum.
 *  Becarefull, key from 10001 are used by SeedMessengerTemplateEmailEnum.
 *  To complete if needed.
 */
class MessengerTemplateEmailEnum
{
    public const TEMPLATE_EMAIL_CONTACT_SENT = 1;
    public const TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT = 2;

    /** @var array */
    public static $data = [
        self::TEMPLATE_EMAIL_CONTACT_SENT => [
            'name' => 'TEMPLATE_EMAIL_CONTACT_SENT',
            'dev_key' => self::TEMPLATE_EMAIL_CONTACT_SENT,
            'subject' => 'Contact',
            'receivers' => 'receiver@mon-site.fr',
            'is_system' => false,
            'is_notification' => false,
            'sender_email' => 'sender@mon-site.fr',
            'help' => '<p><b>{{ message }}</b> : Message</p><p><b>{{ lastname }}</b> : Nom du contact</p><b>{{ firstname }}</b> : Prénom du contact</p><b>{{ email }}</b> : Email du contact</p><b>{{ phonenumber }}</b> : Téléphone du contact</p>',
            'content' => '<p>{{ message }}<br /><br />Nom et pr&eacute;nom : {{ lastname }} {{ firstname }}<br /><br />Email : {{ email }}<br /><br />T&eacute;l&eacute;phone : {{ phonenumber }}</p>',
        ],
        self::TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT => [
            'name' => 'TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT',
            'dev_key' => self::TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT,
            'subject' => 'Contact',
            'receivers' => 'receiver@mon-site.fr',
            'is_system' => false,
            'is_notification' => false,
            'sender_email' => 'sender@mon-site.fr',
            'help' => '<p>{{ message }}<br /><br /><b>Nom et pr&eacute;nom : </b> {{ lastname }} {{ firstname }}<br /><br /><b>Email : </b> {{ email }}<br /><br /><b>T&eacute;l&eacute;phone : </b> {{ phonenumber }}<br /><br /><b>Sujet avancée : </b> {{ subject }}<br /><br /><b>Image : </b>{{ image }}<br /><br /><b>Document : </b>{{ document }}</p>',
            'content' => '<p>{{ message }}<br /><br /><b>Nom et pr&eacute;nom : </b> {{ lastname }} {{ firstname }}<br /><br /><b>Email : </b> {{ email }}<br /><br /><b>T&eacute;l&eacute;phone : </b> {{ phonenumber }}<br /><br /><b>Sujet avancée : </b> {{ subject }}<br /><br /><b>Image : </b>{{ image }}<br /><br /><b>Document : </b>{{ document }}</p>',
        ],
    ];

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```
## FormType creation

**4 - Creation of the form type that we will use to extend another form type**

```
📃 app/src/Form/ContactAdvancedType.php
```
**4.1 - First we have to get all our constants to work with**
```
💡 In this form we want to get error message and select options
```
```php
// Select error message
$selectErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR);

// Image Constraints Messages
$imageFormatErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR);
$imageSizeErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR);

// Document Constraints Messages
$fileFormatErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR);
$fileSizeErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR);

// Select options
$dataOptions = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS);

// deployment method to inject options into our select
$choices = ['Choisissez un sujet' => null];
$choicesElements = explode(';', (string) $dataOptions);

foreach ($choicesElements as $key => $value) {
    $choices[$value] = $value;
}
```
**4.2 - We will take the main form that we want to extend, and add values**
```php
//To grab the main form
->add('contact', ContactType::class, ['required' => true])

//Add new value to complete the extendation
->add('subject', ChoiceType::class, [
    'required' => true,
    'label' => 'Sujet avancé *',
    'multiple' => false,
    'choices' => $choices,
    'constraints' => [
        new NotBlank([
            'message' => $selectErrorMessage,
        ]),
    ],
])
->add('image', FileType::class, [
    'label' => 'Image',
    'mapped' => false,
    'required' => false,
    'constraints' => [
        new File([
            'maxSize' => '1000k',
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
            ],
            'mimeTypesMessage' => $imageFormatErrorMessage,
            'maxSizeMessage' => $imageSizeErrorMessage,
        ]),
    ],
])
->add('document', FileType::class, [
    'label' => 'Document PDF',
    'mapped' => false,
    'required' => false,
    'constraints' => [
        new File([
            'maxSize' => '2000k',
            'mimeTypes' => [
                'application/pdf',
                'application/x-pdf',
            ],
            'mimeTypesMessage' => $fileFormatErrorMessage,
            'maxSizeMessage' => $fileSizeErrorMessage,
        ]),
    ],
]);
```
**Example**
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form;

use App\Enum\DataEnum;
use Seed\Manager\SeedDataManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactAdvancedType extends AbstractType
{
    /** @var SeedDataManager */
    private $seedDataManager;

    public function __construct(SeedDataManager $seedDataManager)
    {
        $this->seedDataManager = $seedDataManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $selectErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR);

        //Image Constraints Messages
        $imageFormatErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR);
        $imageSizeErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR);

        //Document Constraints Messages
        $fileFormatErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR);
        $fileSizeErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR);

        $dataOptions = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_ADVANCED_MAIL_SUBJECT_OPTIONS);

        $choices = ['Choisissez un sujet' => null];
        $choicesElements = explode(';', (string) $dataOptions);

        foreach ($choicesElements as $key => $value) {
            $choices[$value] = $value;
        }

        $builder
            ->add('contact', ContactType::class, ['required' => true])
            ->add('subject', ChoiceType::class, [
                'required' => true,
                'label' => 'Sujet avancé *',
                'multiple' => false,
                'choices' => $choices,
                'constraints' => [
                    new NotBlank([
                        'message' => $selectErrorMessage,
                    ]),
                ],
            ])
            ->add('image', FileType::class, [
                'label' => 'Image',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => $imageFormatErrorMessage,
                        'maxSizeMessage' => $imageSizeErrorMessage,
                    ]),
                ],
            ])
            ->add('document', FileType::class, [
                'label' => 'Document PDF',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2000k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => $fileFormatErrorMessage,
                        'maxSizeMessage' => $fileSizeErrorMessage,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
```

## TWIG template about the form

**5 - TWIG template creation**
```
📃 app/templates/Frontend/Pages/page-contact-advanced.html.twig
```
**5.1 - Implement this code**
```
💡 To get the style into our previous input form that we extend we have to use "children.contact" and add our field name right after
```
```twig
{% extends '@Seed/Elements/Pages/page-contact.html.twig' %}

{% block contactForm %}
    {{ form_start(form, {class: 'needs-validation'}) }}
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.lastname, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.lastname, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.lastname) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.firstname, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.firstname, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.firstname) }}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.company, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.company, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.company) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.fonction, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.fonction, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.fonction) }}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.email, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.email, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.email) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.children.contact.phonenumber, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.phonenumber, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.phonenumber) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.children.contact.subject, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.subject, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.subject) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.subject, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.subject, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.subject) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.image, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.image, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.image) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.document, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.document, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.document) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.children.contact.message, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.children.contact.message, {attr: {class: 'form-control', style: 'height: 100px;'}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.message) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_widget(form.children.contact.politics) }}
                {{ form_label(form.children.contact.politics, null, {label_attr: {class: ''}}) }}
                <div class="form-error">
                    {{ form_errors(form.children.contact.politics) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <button class="btn btn-primary" type="submit">
                    <i class="bi bi-send-fill mr-1"></i>
                    Envoyer
                </button>
            </div>
        </div>
    {{ form_end(form) }}
{% endblock %}
```
## Method into MailerService

**6 - Go to MailerService**
```
📃 app/src/Service/MailerService.php
```
**6.1 - Implement this code**
```php
/**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function contactAdvancedMailSend(array $data, string $imageUrl = null, string $documentUrl = null): bool
    {
        //Get the email template that we have created
        /** @var SeedMessengerTemplateEmail|null $seedMessengerTemplateEmail */
        $seedMessengerTemplateEmail = $this->seedMessengerTemplateEmailRepository->findOneBy(['dev_key' => MessengerTemplateEmailEnum::TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT]);

        if (null === $seedMessengerTemplateEmail) {
            throw new \Exception('Seed email template not exist');
        }

        //Get the content of messengerTemplateEmail
        $content = $seedMessengerTemplateEmail->getContent();

        // Define every fields
        $email = $data['contact']['email'];
        $subject = $data['subject'];
        $content = $content = str_replace(['{{message}}', '{{ message }}'], $data['contact']['message'] ?? '', (string) $content);
        $content = $content = str_replace(['{{lastname}}', '{{ lastname }}'], $data['contact']['lastname'] ?? '', (string) $content);
        $content = $content = str_replace(['{{firstname}}', '{{ firstname }}'], $data['contact']['firstname'] ?? '', (string) $content);
        $content = $content = str_replace(['{{email}}', '{{ email }}'], $email ?? '', (string) $content);
        $content = $content = str_replace(['{{phonenumber}}', '{{ phonenumber }}'], $data['contact']['phonenumber'] ?? '', (string) $content);
        $content = $content = str_replace(['{{subject}}', '{{ subject }}'], $data['contact']['subject'] ?? '', (string) $content);

        $content = $content = str_replace(['{{image}}', '{{ image }}'], null !== $imageUrl ? sprintf('<a href="%s">%s</a>', $imageUrl, $imageUrl) : 'Non renseigné.', (string) $content);
        $content = $content = str_replace(['{{document}}', '{{ document }}'], null !== $documentUrl ? sprintf('<a href="%s">%s</a>', $documentUrl, $documentUrl) : 'Non renseigné.', (string) $content);

        // Receivers
        $receivers = explode(';', (string) $seedMessengerTemplateEmail->getReceivers());

        // Email creation
        $swiftMessage = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom([$seedMessengerTemplateEmail->getSenderEmail() => $seedMessengerTemplateEmail->getSenderName()])
            ->setReplyTo($seedMessengerTemplateEmail->getReplyTo())
            ->setTo($receivers)
            ->setBody($this->twig->render($this->getTemplate($seedMessengerTemplateEmail), [
                'title' => $subject,
                'content' => $content,
            ]))
            ->setContentType('text/html');

        // When we are into another environment than development we specify that we want files/images into attachments
        if ('dev' !== strtolower($this->kernel->getEnvironment())) {
            if (null !== $imageUrl) {
                $swiftMessage->attach(\Swift_Attachment::fromPath($imageUrl));
            }

            if (null !== $documentUrl) {
                $swiftMessage->attach(\Swift_Attachment::fromPath($documentUrl));
            }
        }

        // Send the message
        return $this->send($swiftMessage);
    }
```
## Creation of the method in the controller
**7 - Go to the Controller**
```
📃 app/src/Controller/Frontend/IndexController.php
```
**7.1 - Implement this code**
```php
/**
     * @Route("/contact-advanced", name="frontend_contact_advanced")
     */
    public function contactAdvanced(
        Request $request,
        // Injection of the MailerService: To work on our mail creation
        MailerService $mailerService,
        // Injection of the mediaUploaderService: To work on our images / files
        MediaUploaderService $mediaUploaderService
    ): Response {
        // Getting current seedWebsite
        $currentSeedWebsite = $this->getCurrentSeedWebsite();

        // Getting seed page
        $seedPage = $this->getSeedPageFromDataDevKey(DataEnum::DATA_PAGE_CONTACT_ADVANCED_DEV_KEY);

        // Create form
        $form = $this->createForm(ContactAdvancedType::class, null, ['action' => $this->generateUrl('frontend_contact_advanced')]);
        if ($form->handleRequest($request)->isSubmitted()) {
            if ($form->isValid()) {
                // Getting image data
                /** @var UploadedFile|null $imageFile */
                $imageFile = $form->get('image')->getData();

                // Getting document data
                /** @var UploadedFile|null $documentFile */
                $documentFile = $form->get('document')->getData();

                // If an image exists we upload the file using constraints and prepare the url
                if (null !== $imageFile) {
                    // Size constraint is in octets
                    $imageFile = $mediaUploaderService->uploadImage($imageFile, '/upload/attachments', ['size' => 1000000, 'mimes' => ['image/png', 'image/jpeg']]);

                    // Prepare the full url
                    $imageUrl = $currentSeedWebsite->getBaseUrl().$imageFile[1].'/'.$imageFile[2];
                }

                // If a document exists we upload the file using constraints and prepare the url
                if (null !== $documentFile) {
                    // Size constraint is in octets
                    $documentFile = $mediaUploaderService->uploadDocument($documentFile, '/upload/attachments', ['size' => 2000000, 'mimes' => ['application/pdf', 'application/x-pdf']]);

                    // Get document full url
                    $documentPath = $documentFile[1].'/'.$documentFile[2];

                    // Prepare the full url
                    $documentUrl = $currentSeedWebsite->getBaseUrl().$documentFile[1].'/'.$documentFile[2];
                }

                // Send data into our MailerService method (form data and imagse / documents if exist)
                $mailerService->contactAdvancedMailSend((array) $form->getData(), $imageUrl ?? null, $documentUrl ?? null);

                // Redirect the user into the success page
                return $this->redirectFromDataDevKey(DataEnum::DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY);
            }
            // Display message in case of error
            $this->addFlash('alert', $this->getSeedDataValue(DataEnum::DATA_MESSAGE_CONTACT_ADVANCED_ERROR));
        }

        $viewParameters = [];
        $viewParameters['form'] = $form->createView();
        $viewParameters['page'] = $seedPage;

        // render the template that we using
        return $this->renderSeed($seedPage, 'Frontend/Pages/page-contact-advanced.html.twig', $viewParameters);
    }
```

## Update our work into database and back office
**8 - Run the following command***
```
💡 This will fill the database and add data into the back office
```
```shell
make update
```

## Back Office
**9 - Go to the back office into your navigator**
**9.1 - Form page creation**
```
🌐 Pages > Create page
```
**Page content**
```
⚫️ Name (ex: Advanced Form)
⚫️ Title (ex: Advanced Form)
⚫️ Slug (ex: contact-advanced)
⚫️ Dev section > get the key (ex: 29)
⚫️ Symfony route (ex: frontend_contact_advanced)
❗️ Make sur the symfony route match with the route defined into our controller
⚫️ Template (ex: Frontend/Pages/page-contact-advanced.html.twig)
❗️ Make sur the template match with the our template that we've created
```
**9.2 - Data related to the page**
```
🌐 Data > Pages
```
```
💡 Edit the DATA_PAGE_CONTACT_ADVANCED_DEV_KEY
```
**Data page content**
```
⚫️ Value (ex: 29)
❗️ Make sure the value is the same as the page we want to relate to
❗️ Don't forget to publish the page
```
**9.3 - Form on success page creation**
```
💡 Do exactly the same thing than the previous page but with DATA_PAGE_CONTACT_ADVANCED_ON_SUCCESS_DEV_KEY (create page, relate the page to the data)
```
**9.4 - Data related to error messages**
```
🌐 Data > Messages
```
```
💡 Since the "make update" we can observe that we have new keys, feel free to edit the value message

⚫️ DATA_MESSAGE_FORM_ADVANCED_SELECT_ERROR 
⚫️ DATA_MESSAGE_FORM_ADVANCED_IMAGE_FORMAT_ERROR
⚫️ DATA_MESSAGE_FORM_ADVANCED_IMAGE_SIZE_ERROR
⚫️ DATA_MESSAGE_FORM_ADVANCED_FILE_FORMAT_ERROR
⚫️ DATA_MESSAGE_FORM_ADVANCED_FILE_SIZE_ERROR
⚫️ DATA_MESSAGE_CONTACT_ADVANCED_ERROR
```

**9.5 - Data related to email template**
```
🌐 Communication > Email templates
```
```
💡 Since the "make update" we can observe that we have new key which is related to our email template that we've created

⚫️ TEMPLATE_EMAIL_CONTACT_ADVANCED_SENT
```
**Preview**
```
Contact
{{ message }}

Nom et prénom : {{ lastname }} {{ firstname }}

Email : {{ email }}

Téléphone : {{ phonenumber }}

Sujet avancée : {{ subject }}

Image : {{ image }}

Document : {{ document }}
```

## Result

```
Form
```
![image](./images/form.png)
```
Error Message
```
![image](./images/error-message.png)
```
Form redirection to success page
```
![image](./images/form-success.png)
```
Mail
```
![image](./images/mail.png)




