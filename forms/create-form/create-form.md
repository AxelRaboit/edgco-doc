# Create form

## Situation

**We want to create a form with**
```
⚫️ Text inputs
⚫️ Select inputs
⚫️ Document upload
⚫️ Image upload

```
## Creation of constants
**1 - Constants declaration of pags and error message**
```
📃 app/src/Enum/DataEnum.php
```
```
💡 In this part we will define all the constants to display error messages, pages, and select options
```
```php
// Constant with select options
public const DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS = 4;

// Constant with another select options
public const DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS = 5;

// Constant for the contact page
public const DATA_PAGE_FORM_TEST_DEV_KEY = 2;

// Constant for the page in case of success
public const DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY = 3;

//// Constant error message (globally way)
public const DATA_MESSAGE_FORM_TEST_ERROR = 0;

// Constant error message in case we didn't choose a value
public const DATA_MESSAGE_FORM_TEST_SELECT_ERROR = 1;
```
**And declare every constants with their content**
```php
/** @var array */
    public static $data = [
        self::DATA_PAGE_FORM_TEST_DEV_KEY => [
            // Name: Which is the name of the constant that we will find into the back office
            'name' => 'DATA_PAGE_FORM_TEST_DEV_KEY',
            // dev_key: Ref to the constant on the top
            'dev_key' => self::DATA_PAGE_FORM_TEST_DEV_KEY,
            // Default value, in this page case we assign a default page that we will change the value in the back office after for associate a page
            'value' => PageEnum::PAGE_FORM_TEST_DEV_KEY_DEFAULT,
            // System data
            'is_system' => true,
            // Sensitive aata
            'is_sensitive' => false,
            // Category name to find the constant into the back office
            'category' => 'Pages',
        ],
        //...
    ];
```
**Example**
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 * Data - Enum.
 * Becarefull, key from 10001 are used by SeedDataEnum.
 * To complete if needed.
 */
class DataEnum
{
    public const DATA_PAGE_FORM_TEST_DEV_KEY = 2;
    public const DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY = 3;

    public const DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS = 5;
    public const DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS = 6

    public const DATA_MESSAGE_FORM_TEST_SELECT_ERROR = 1;
    public const DATA_MESSAGE_FORM_TEST_ERROR = 0;

    /** @var array */
    public static $data = [
        self::DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS => [
            'name' => 'DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS',
            'dev_key' => self::DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS,
            'value' => 'item 1;item 2;item 3',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Contact',
            'help' => 'Options séparées par des \';\'',
        ],
        self::DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS => [
            'name' => 'DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS',
            'dev_key' => self::DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS,
            'value' => 'object 1;object 2;object 3;object 4;object 5',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Contact',
            'help' => 'Options séparées par des \';\'',
        ],

        self::DATA_PAGE_FORM_TEST_DEV_KEY => [
            'name' => 'DATA_PAGE_FORM_TEST_DEV_KEY',
            'dev_key' => self::DATA_PAGE_FORM_TEST_DEV_KEY,
            'value' => PageEnum::PAGE_FORM_TEST_DEV_KEY_DEFAULT,
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Pages',
        ],

        self::DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY => [
            'name' => 'DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY',
            'dev_key' => self::DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY,
            'value' => PageEnum::PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY_DEFAULT,
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Pages',
        ],
        self::DATA_MESSAGE_FORM_TEST_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_TEST_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_TEST_ERROR,
            'value' => 'Une erreur s\'est produite',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],

        self::DATA_MESSAGE_FORM_TEST_SELECT_ERROR => [
            'name' => 'DATA_MESSAGE_FORM_TEST_SELECT_ERROR',
            'dev_key' => self::DATA_MESSAGE_FORM_TEST_SELECT_ERROR,
            'value' => 'Veuillez renseigner une valeur',
            'is_system' => true,
            'is_sensitive' => false,
            'category' => 'Messages',
        ],
    ];

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```
**2 - Go to the PageEnum**
```
📃 app/src/Enum/PageEnum.php
```
```
💡 These two declarations are related to the two previous pages that we have created (which is form page and page in case of success)
```
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 *  Page - Enum.
 */
class PageEnum
{
    public const PAGE_FORM_TEST_DEV_KEY_DEFAULT = 0;
    public const PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY_DEFAULT = 1;

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```
**3 - Go to the MessengerTemplateEmailEnum**
```
📃 app/src/Enum/MessengerTemplateEmailEnum.php
```
**3.1 - Constants declaration of the messenger template email**
```
💡 These constants will be available in the back office after declarations
```
**Constant's content**
```php
// Constant related to the email template taht we will find into the back office after declaration
public const TEMPLATE_EMAIL_FORM_TEST_SENT = 2;

/** @var array */
    public static $data = [
        self::TEMPLATE_EMAIL_FORM_TEST_SENT => [
            // Name: Which is the name of the constant that we will find into the back office
            'name' => 'TEMPLATE_EMAIL_FORM_TEST_SENT',
            // dev_key: Ref to the constant on the top
            'dev_key' => self::TEMPLATE_EMAIL_FORM_TEST_SENT,
            // Mail subject
            'subject' => 'Form Test',
            // Receivers email
            'receivers' => 'receiver@mon-site.fr',
            // Data system
            'is_system' => false,
            // Active notification
            'is_notification' => false,
            // Sender email
            'sender_email' => 'sender@mon-site.fr',
            // Helper
            'help' => '<p><b>{{ message }}</b> : Message</p><p><b>{{ lastname }}</b> : Nom du contact</p><b>{{ firstname }}</b> : Prénom du contact</p><b>{{ email }}</b> : Email du contact</p><b>{{ phonenumber }}</b> : Téléphone du contact<b>{{ subject_test }}</b> : Sujet Test</p>',
            // Email content (This is the email structure, the variables will be injected inside "{{ content }}" )
            'content' => '<p>{{ message }}<br /><br />Nom et pr&eacute;nom : {{ lastname }} {{ firstname }}<br /><br />Email : {{ email }}<br /><br />T&eacute;l&eacute;phone : {{ phonenumber }}<br /><br />Sujet Test: {{ subject_test }}</p>',
        ],
    ];
```
**Example**
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Enum;

/**
 *  MessengerTemplateEmail - Enum.
 *  Becarefull, key from 10001 are used by SeedMessengerTemplateEmailEnum.
 *  To complete if needed.
 */
class MessengerTemplateEmailEnum
{
    public const TEMPLATE_EMAIL_FORM_TEST_SENT = 2;

    /** @var array */
    public static $data = [
        self::TEMPLATE_EMAIL_FORM_TEST_SENT => [
            'name' => 'TEMPLATE_EMAIL_FORM_TEST_SENT',
            'dev_key' => self::TEMPLATE_EMAIL_FORM_TEST_SENT,
            'subject' => 'Form Test',
            'receivers' => 'receiver@mon-site.fr',
            'is_system' => false,
            'is_notification' => false,
            'sender_email' => 'sender@mon-site.fr',
            'help' => '<p><b>{{ message }}</b> : Message</p><p><b>{{ lastname }}</b> : Nom du contact</p><b>{{ firstname }}</b> : Prénom du contact</p><b>{{ email }}</b> : Email du contact</p><b>{{ phonenumber }}</b> : Téléphone du contact<b>{{ subject_test }}</b> : Sujet Test</p>',
            'content' => '<p>{{ message }}<br /><br />Nom et pr&eacute;nom : {{ lastname }} {{ firstname }}<br /><br />Email : {{ email }}<br /><br />T&eacute;l&eacute;phone : {{ phonenumber }}<br /><br />Sujet Test: {{ subject_test }}</p>',
        ],
    ];

    /**
     * @return array
     */
    public static function getAvailable()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return array_values((array) $reflectionClass->getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $reflectionClass = new \ReflectionClass(self::class);

        return (array) $reflectionClass->getConstants();
    }
}
```
## FormType creation
**4 - Creation of the form type**
```
📃 app/src/Form/FormTestType.php
```
**4.1 - First we have to get all our constants to work with**
```
💡 In this form we want to get error message and select options
```
```php
// Get select error message
$selectErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_TEST_SELECT_ERROR);

// Get options for the first select
$dataOptions = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS);
$choices = ['Choisissez un sujet' => null];
$choicesElements = explode(';', (string) $dataOptions);

foreach ($choicesElements as $key => $value) {
    $choices[$value] = $value;
}

// Get options for the second select
$dataOptionsTest = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS);
$choicesTest = ['Choisissez un sujet test' => null];
$choicesElementsTest = explode(';', (string) $dataOptionsTest);

foreach ($choicesElementsTest as $key => $value) {
    $choicesTest[$value] = $value;
}
```
**4.2 - Define all the input values**
```php
->add('firstname', TextType::class, ['required' => true, 'label' => 'Prénom *'])
->add('lastname', TextType::class, ['required' => true, 'label' => 'Nom *'])
->add('email', EmailType::class, ['required' => true, 'label' => 'E-mail *'])
->add('phonenumber', NumberType::class, ['required' => true, 'label' => 'Téléphone *'])
->add('company', TextType::class, ['required' => false, 'label' => 'Société'])
->add('fonction', TextType::class, ['required' => false, 'label' => 'Fonction'])

->add('subject', ChoiceType::class, [
    'required' => true,
    'label' => 'Sujet *',
    'multiple' => false,
    'choices' => $choices,
    'constraints' => [
        new NotBlank([
            'message' => $selectErrorMessage,
        ]),
    ],
])

->add('subject_test', ChoiceType::class, [
    'required' => true,
    'label' => 'Sujet Test *',
    'multiple' => false,
    'choices' => $choicesTest,
    'constraints' => [
        new NotBlank([
            'message' => $selectErrorMessage,
        ]),
    ],
])
->add('message', TextareaType::class, ['required' => true, 'label' => 'Message *'])
->add('politics', CheckboxType::class, ['required' => true, 'label' => 'J’accepte la politique de confidentialité *']);
```
**Example**
```php
<?php

declare(strict_types=1);

/*
 * Copyright (C) EDGCo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Form;

use App\Enum\DataEnum;
use Seed\Manager\SeedDataManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormTestType extends AbstractType
{
    /** @var SeedDataManager */
    private $seedDataManager;

    public function __construct(SeedDataManager $seedDataManager)
    {
        $this->seedDataManager = $seedDataManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        // Get select error message
        $selectErrorMessage = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_TEST_SELECT_ERROR);

        // Get options for the first select
        $dataOptions = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_MAIL_TEST_SUBJECT_OPTIONS);
        $choices = ['Choisissez un sujet' => null];
        $choicesElements = explode(';', (string) $dataOptions);
        foreach ($choicesElements as $key => $value) {
            $choices[$value] = $value;
        }

        // Get options for the second select
        $dataOptionsTest = $this->seedDataManager->getSeedDataValue(DataEnum::DATA_FORM_MAIL_TEST_OTHER_SUBJECT_OPTIONS);
        $choicesTest = ['Choisissez un sujet test' => null];
        $choicesElementsTest = explode(';', (string) $dataOptionsTest);
        foreach ($choicesElementsTest as $key => $value) {
            $choicesTest[$value] = $value;
        }

        $builder
            ->add('firstname', TextType::class, ['required' => true, 'label' => 'Prénom *'])
            ->add('lastname', TextType::class, ['required' => true, 'label' => 'Nom *'])
            ->add('email', EmailType::class, ['required' => true, 'label' => 'E-mail *'])
            ->add('phonenumber', NumberType::class, ['required' => true, 'label' => 'Téléphone *'])
            ->add('company', TextType::class, ['required' => false, 'label' => 'Société'])
            ->add('fonction', TextType::class, ['required' => false, 'label' => 'Fonction'])

            ->add('subject', ChoiceType::class, [
                'required' => true,
                'label' => 'Sujet *',
                'multiple' => false,
                'choices' => $choices,
                'constraints' => [
                    new NotBlank([
                        'message' => $selectErrorMessage,
                    ]),
                ],
            ])

            ->add('subject_test', ChoiceType::class, [
                'required' => true,
                'label' => 'Sujet Test *',
                'multiple' => false,
                'choices' => $choicesTest,
                'constraints' => [
                    new NotBlank([
                        'message' => $selectErrorMessage,
                    ]),
                ],
            ])
            ->add('message', TextareaType::class, ['required' => true, 'label' => 'Message *'])
            ->add('politics', CheckboxType::class, ['required' => true, 'label' => 'J’accepte la politique de confidentialité *']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
```
## TWIG template about the form
**5 - TWIG template creation**
```
📃 app/templates/Frontend/Pages/form-test.html.twig

```
**5.1 - Implement this code**
```twig
{% extends '@Seed/Elements/Pages/page-contact.html.twig' %}

{% block contactForm %}
    {{ form_start(form, {class: 'needs-validation'}) }}
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.lastname, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.lastname, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.lastname) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.firstname, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.firstname, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.firstname) }}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.company, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.company, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.company) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.fonction, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.fonction, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.fonction) }}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                {{ form_label(form.email, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.email, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.email) }}
                </div>
            </div>
            <div class="col-md-6 mb-3">
                {{ form_label(form.phonenumber, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.phonenumber, {attr: {class: 'form-control'}}) }}
                <div class="form-error">
                    {{ form_errors(form.phonenumber) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.subject, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.subject, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.subject) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.subject_test, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.subject_test, {attr: {class: 'custom-select'}}) }}
                <div class="form-error">
                    {{ form_errors(form.subject_test) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_label(form.message, null, {label_attr: {class: ''}}) }}
                {{ form_widget(form.message, {attr: {class: 'form-control', style: 'height: 100px;'}}) }}
                <div class="form-error">
                    {{ form_errors(form.message) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                {{ form_widget(form.politics) }}
                {{ form_label(form.politics, null, {label_attr: {class: ''}}) }}
                <div class="form-error">
                    {{ form_errors(form.politics) }}
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <button class="btn btn-primary" type="submit">
                    <i class="bi bi-send-fill mr-1"></i>
                    Envoyer
                </button>
            </div>
        </div>
    {{ form_end(form) }}
{% endblock %}
```
## Method into MailerService
**6 - Go to MailerService**
```
📃 app/src/Service/MailerService.php
```
**6.1 - Implement this code**
```php
/**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function formTestMailSend(array $data): bool
    {
        //Get the email template that we have created
        /** @var SeedMessengerTemplateEmail|null $seedMessengerTemplateEmail */
        $seedMessengerTemplateEmail = $this->seedMessengerTemplateEmailRepository->findOneBy(['dev_key' => MessengerTemplateEmailEnum::TEMPLATE_EMAIL_FORM_TEST_SENT]);

        if (null === $seedMessengerTemplateEmail) {
            throw new \Exception('Seed email template not exist');
        }

        //Get the content of messengerTemplateEmail
        $content = $seedMessengerTemplateEmail->getContent();

        // Define every fields
        $email = $data['email'];
        $subject = $data['subject'];
        $content = $content = str_replace(['{{message}}', '{{ message }}'], $data['message'] ?? '', (string) $content);
        $content = $content = str_replace(['{{lastname}}', '{{ lastname }}'], $data['lastname'] ?? '', (string) $content);
        $content = $content = str_replace(['{{firstname}}', '{{ firstname }}'], $data['firstname'] ?? '', (string) $content);
        $content = $content = str_replace(['{{email}}', '{{ email }}'], $email ?? '', (string) $content);
        $content = $content = str_replace(['{{phonenumber}}', '{{ phonenumber }}'], $data['phonenumber'] ?? '', (string) $content);
        $content = $content = str_replace(['{{subject_test}}', '{{ subject_test }}'], $data['subject_test'] ?? '', (string) $content);

        // Receivers
        $receivers = explode(';', (string) $seedMessengerTemplateEmail->getReceivers());

        // Email
        $swiftMessage = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom([$seedMessengerTemplateEmail->getSenderEmail() => $seedMessengerTemplateEmail->getSenderName()])
            ->setReplyTo($seedMessengerTemplateEmail->getReplyTo())
            ->setTo($receivers)
            ->setBody($this->twig->render($this->getTemplate($seedMessengerTemplateEmail), [
                'title' => $subject,
                'content' => $content,
            ]))
            ->setContentType('text/html');

        // Send
        return $this->send($swiftMessage);
    }
```
## Creation of the method in the controller
**7 - Go to the Controller**
```
📃 app/src/Controller/Frontend/IndexController.php
```
**7.1 - Implement this code**
```php
/**
     * @Route({
     *     "fr": "/form-test",
     *     "en": "/en/form-test"
     * }, name="frontend_form_test")
     */
    public function formTest(Request $request, MailerService $mailerService, Environment $twig): Response
    {
        // Getting page
        $page = $this->getSeedPageFromDataDevKey(DataEnum::DATA_PAGE_FORM_TEST_DEV_KEY);

        // Create form
        $form = $this->createForm(FormTestType::class, null, ['action' => $this->generateUrl('frontend_form_test')]);
        if ($form->handleRequest($request)->isSubmitted()) {
            if ($form->isValid()) {
                $mailerService->formTestMailSend((array) $form->getData());

                return $this->redirectFromDataDevKey(DataEnum::DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY);
            }
            $this->addFlash('alert', $this->getSeedDataValue(DataEnum::DATA_MESSAGE_FORM_TEST_ERROR));
        }

        $viewParameters = [];
        $viewParameters['form'] = $form->createView();
        $viewParameters['page'] = $page;

        // Getting the twig template related to the form
        return $this->renderSeed($page, 'Frontend/Pages/form-test.html.twig', $viewParameters);
    }
```
## Update our work into database and back office
**8 - Run the following command***
```
💡 This will fill the database and add data into the back office
```
```shell
make update
```
## Back Office
**9 - Go to the back office into your navigator**
**9.1 - Form page creation**
```
🌐 Pages > Create page
```
**Page content**
⚫️ Name (ex: Contact Form)
⚫️ Title (ex: Contact Form)
⚫️ Slug (ex: form-test)
⚫️ Dev section > get the key (ex: 25)
⚫️ Symfony route (ex: frontend_form_test)
❗️ Make sur the symfony route match with the route defined into our controller
⚫️ Template (ex: Frontend/Pages/form-test.html.twig)
❗️ Make sur the template match with the our template that we've created
**9.2 - Data related to the page**
```
🌐 Data > Pages
```
```
💡 Edit the DATA_PAGE_FORM_TEST_DEV_KEY
```
**Data page content**
```
⚫️ Value (ex: 25)
❗️ Make sure the value is the same as the page we want to relate to
❗️ Don't forget to publish the page
```
**9.3 - Form on success page creation**
```
💡 Do exactly the same thing than the previous page but with DATA_PAGE_FORM_TEST_ON_SUCCESS_DEV_KEY (create page, relate the page to the data)
```
**9.4 - Data related to error messages**
```
🌐 Data > Messages
```
```
💡 Since the "make update" we can observe that we have new keys, feel free to edit the value message

⚫️ DATA_MESSAGE_FORM_TEST_ERROR
⚫️ DATA_MESSAGE_FORM_TEST_SELECT_ERROR
```
**9.5 - Data related to email template**
```
🌐 Communication > Email templates
```
```
💡 Since the "make update" we can observe that we have new key which is related to our email template that we've created

⚫️ TEMPLATE_EMAIL_FORM_TEST_SENT
```

## Result
```
Error Message
```
![image](./images/form-error.png)
```
Form redirection to success page
```
![image](./images/form-success.png)
```
Mail
```
![image](./images/mail.png)